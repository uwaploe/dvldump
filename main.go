// Dvldump dumps the contents of an DVL MessagePack archive file as
// ndJSON or raw binary
package main

import (
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"

	dvl "bitbucket.org/uwaploe/go-dvl"
	must "bitbucket.org/uwaploe/go-must"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: dvldump [options] infile outfile

Reads a MessagePack format DVL records from INFILE and writes them to
OUTFILE in newline-delimited JSON format or raw binary.
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	appendMode = flag.Bool("append", false,
		"If true, append to OUTFILE rather than overwrite")
	useRaw = flag.Bool("raw", false,
		"If true, OUTFILE is in raw binary format")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	fin, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("Open %q failed: %v", args[0], err)
	}

	var flags int

	if *appendMode {
		flags = os.O_APPEND | os.O_CREATE | os.O_WRONLY
	} else {
		flags = os.O_CREATE | os.O_WRONLY
	}

	fout, err := os.OpenFile(args[1], flags, 0644)
	if err != nil {
		log.Fatalf("Open %q failed: %v", args[1], err)
	}

	rdr := must.NewDvlReader(fin)
	count := int(0)
	eol := []byte("\n")
	for rdr.Scan() {
		rec := rdr.Record()
		if *useRaw {
			if err := binary.Write(fout, dvl.ByteOrder, rec); err != nil {
				log.Fatalf("Write error on record %d: %v", count, err)
			}
		} else {
			b, err := json.Marshal(rec)
			if err != nil {
				log.Fatalf("JSON encoding failed: %v", err)
			}
			fout.Write(b)
			_, err = fout.Write(eol)
			if err != nil {
				log.Fatalf("Write error on record %d: %v", count, err)
			}
		}
		count++
	}

	if err := rdr.Err(); err != nil {
		log.Fatalf("Read error on record %d: %v", count, err)
	}
	log.Printf("Read %d records", count)
}
