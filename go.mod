module bitbucket.org/uwaploe/dvldump

require (
	bitbucket.org/uwaploe/go-dvl v0.9.1
	bitbucket.org/uwaploe/go-ins v1.1.2
	bitbucket.org/uwaploe/go-must v0.2.2
)
