# Dump DVL Archive Records

Dvldump reads a MuST DVL archive file and dumps the records in
[newline-delimited JSON](http://ndjson.org) (ndJSON) or raw binary format.

## Installation

Visit the download page of this repository to obtain a compressed-tar
archive of the compiled binary for your OS (Linux, Windows, or MacOS).

## Usage

The `-help` option displays a usage message

``` shellsession
$ dvldump -help
Usage: dvldump [options] infile outfile

Reads a MessagePack format DVL records from INFILE and writes them to
OUTFILE in newline-delimited JSON format or raw binary.
  -append
    	If true, append to OUTFILE rather than overwrite
  -raw
    	If true, OUTFILE is in raw binary format
  -version
    	Show program version information and exit
```

Convert a file to njJSON.

``` shellsession
$ dvldump dvl-20190806-22.mpk dvl.ndjson
2019/08/19 12:18:11 Read 24798 records
$ head -n 2 dvl.ndjson
{"HeaderId":125,"DataId":0,"Nbytes":45,"SysCfg":147,"VelBtm":[18,1881,-38,0],"RngBtm":[3070,3085,3100,3040],"BtmStatus":0,"Vel":[-32768,-32768,-32768,-32768],"RefStart":0,"RefEnd":0,"RefStatus":15,"T":{"Hour":22,"Min":0,"Sec":0,"Hsec":3},"BitResult":0,"SndSpeed":1448,"Temp":-6,"Checksum":2699}
{"HeaderId":125,"DataId":0,"Nbytes":45,"SysCfg":147,"VelBtm":[14,1885,-34,11],"RngBtm":[3055,3085,3115,3040],"BtmStatus":0,"Vel":[-32768,-32768,-32768,-32768],"RefStart":0,"RefEnd":0,"RefStatus":15,"T":{"Hour":22,"Min":0,"Sec":0,"Hsec":23},"BitResult":0,"SndSpeed":1448,"Temp":-6,"Checksum":2734}
$
```

Convert a file to binary.

``` shellsession
$ dvldump -raw=true dvl-20190806-22.mpk dvl.bin
2019/08/19 12:20:37 Read 24798 records
$ ls -l dvl.bin
-rw-r--r-- 1 mike 1165506 2019-08-19 12:20 dvl.bin
$
```
